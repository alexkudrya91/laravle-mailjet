# Laravel Mailjet
**Install**

```shell
composer require alex-kudrya/laravel-mailjet
```

**Add provider to `config/app.php`**

```php
// config/app.php

'providers' => [
    ...

    /*
     * Package Service Providers...
     */

    \AlexKudrya\LaravelMailjet\Providers\MailJetServiceProvider::class,

     ...

]
```

**Add variables to `.env`**

```
MAILJET_API_KEY={Your mailjet api_key}
MAILJET_SECRET={Your mailjet secret_key}
```

**Add `mailjet` mailer to `mailers` array in `config/mail.php`**

```php
// config/mail.php

'mailers' => [
    ...

    'mailjet' => [
        'transport' => 'mailjet',
        'api_key' => env('MAILJET_API_KEY'),
        'secret_key' => env('MAILJET_SECRET'),
    ],

    ...
]

```

**Change primary mailer in `.env`**

```
MAIL_MAILER=mailjet
```
**Optionally you can change default mailer in `config/mail.php`**

```php
// config/mail.php


'default' => env('MAIL_MAILER', 'mailjet'),


```

**That's all, now you can send your emails!**
