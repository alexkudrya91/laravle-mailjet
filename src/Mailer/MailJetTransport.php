<?php

namespace AlexKudrya\LaravelMailjet\Mailer;

use Mailjet\Resources;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractTransport;
use Symfony\Component\Mime\MessageConverter;

class MailJetTransport extends AbstractTransport
{
    protected \Mailjet\Client $client;

    /**
     * Create a new Mailjet transport instance.
     */
    public function __construct() {
        parent::__construct();

        $this->client = new \Mailjet\Client(
            key: config('mail.mailers.mailjet.api_key'),
            secret: config('mail.mailers.mailjet.secret_key'),
            call: true,
            settings: ['version' => 'v3.1']
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function doSend(SentMessage $message): void
    {
        $email = MessageConverter::toEmail($message->getOriginalMessage());

        $sender = $message->getEnvelope()->getSender();

        $new_message = [];

        $new_message['From']['Email'] = $sender->getAddress();
        $new_message['From']['Name'] = $sender->getEncodedName();

        foreach ($email->getTo() as $recipient) {
            $new_message['To'][] = [
                'Email' => $recipient->getAddress(),
                'Name' => $recipient->getEncodedName()
            ];
        }

        foreach ($email->getBcc() as $recipient) {
            $new_message['Bcc'][] = [
                'Email' => $recipient->getAddress(),
                'Name' => $recipient->getEncodedName()
            ];
        }

        foreach ($email->getCc() as $recipient) {
            $new_message['Cc'][] = [
                'Email' => $recipient->getAddress(),
                'Name' => $recipient->getEncodedName()
            ];
        }

        $new_message['Subject'] = $email->getSubject();
        $new_message['HTMLPart'] = $email->getHtmlBody();
        $new_message['TextPart'] = $email->getTextBody();

        $body = [
            'Messages' => [$new_message]
        ];

        $this->client->post(Resources::$Email, ['body' => $body]);
    }

    /**
     * Get the string representation of the transport.
     */
    public function __toString(): string
    {
        return 'mailjet';
    }
}
