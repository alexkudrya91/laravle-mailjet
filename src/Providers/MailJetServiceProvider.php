<?php

namespace AlexKudrya\LaravelMailjet\Providers;

use AlexKudrya\LaravelMailjet\Mailer\MailJetTransport;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;

class MailJetServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Mail::extend('mailjet', function (array $config = []) {
            return new MailJetTransport();
        });
    }
}
